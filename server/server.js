const express = require('express');
const mongoose = require('mongoose');
const Article = require('./api/models/Article');
const bodyParser = require('body-parser');

var app = express(),
  port = process.env.PORT || 3000;

mongoose.Promise = global.Promise;

var uri = 'mongodb://localhost/Articledb';
var db = mongoose.createConnection(uri);

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/articleRoutes');
routes(app);



app.listen(port);

console.log('article RESTful API server started on: ' + port);
