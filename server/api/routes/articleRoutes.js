'use strict';
module.exports = function(app) {
  var articles = require('../controllers/articleController');

  app.route('/articles')
    .get(articles.listAllArticles)
    .post(articles.createArticle);

  app.route('/articles/search/:term')
    .get(articles.listArticlesContainingTerm);

  app.route('/articles/:articleId')
    .get(articles.readArticle)
    .put(articles.updateArticle)
    .delete(articles.deleteArticle);
};
