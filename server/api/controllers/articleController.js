'use strict';


const simpledb = require('mongoose-simpledb');
const path = require('path');
const db = simpledb.init({
  modelsDir: path.join(__dirname, '..', 'models')
}, function (err, db) {
    if (err) return console.error(err);
});

exports.listAllArticles = function(req, res) {
  db.Article.find({}, function(err, article) {
    if (err)
      res.send(err);
    res.json(article);
  });
};

exports.listArticlesContainingTerm = function(req, res) {
  var likeTerm = new RegExp(req.params.term, 'i');
  db.Article.find({
    $or: [
      { 'title': likeTerm },
      { 'content': likeTerm }
    ]
  }, function(err, articles) {
    if (err)
      res.send(err);
    res.json(articles);
  }).limit(10);
}


exports.createArticle = function(req, res) {
  var article = new db.Article(req.body);
  article.save(function(err, article) {
    if (err)
      res.send(err);
    res.json(article);
  });
};


exports.readArticle = function(req, res) {
  db.Article.findById(req.params.articleId, function(err, article) {
    if (err)
      res.send(err);
    res.json(article);
  });
};


exports.updateArticle = function(req, res) {
  db.Article.findOneAndUpdate({_id: req.params.articleId}, req.body, {new: true}, function(err, article) {
    if (err)
      res.send(err);
    res.json(article);
  });
};


exports.deleteArticle = function(req, res) {


  db.Article.remove({
    _id: req.params.articleId
  }, function(err, article) {
    if (err)
      res.send(err);
    res.json({ message: 'Article successfully deleted' });
  });
};
