const ObjectId = require('mongoose-simpledb').Types.ObjectId;

exports.schema = {
  title: {
    type: String,
    required: 'Title is required'
  },
  content: {
    type: String,
    required: 'Content is required'

  },
  created_date: {
    type: Date,
    default: Date.now
  }
};
