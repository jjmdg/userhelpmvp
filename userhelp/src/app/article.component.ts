import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Location } from '@angular/common';
import 'rxjs/add/operator/switchMap';

import { FieldsetModule } from 'primeng/primeng';

import { ArticleSearchService } from './article-search.service';
import { Article } from './article';


@Component({
  selector: 'article',
  templateUrl: './article.component.html'
})
export class ArticleComponent implements OnInit {

  article: Article;

  @ViewChild('content') articleContent: ElementRef;

  constructor(
    private articleSearchService: ArticleSearchService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.route.paramMap
    .switchMap((params: ParamMap) => this.articleSearchService.getArticle(params.get('id')))
    .subscribe(article => {
      this.article = article;
      this.articleContent.nativeElement.innerHTML = this.article.content;
    });
  }

}
