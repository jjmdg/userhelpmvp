import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';

import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Article }           from './article';

@Injectable()
export class ArticleSearchService {
  articleRestUrl: string = 'api';

  constructor(private http: Http) {}

  search(term: string): Observable<Article[]> {
    return this.http
               .get(`${this.articleRestUrl}/articles/search/${term}`)
               .map(response => response.json() as Article[]);
  }

  getArticle(id: string): Promise<Article> {
    return this.http
      .get(`${this.articleRestUrl}/articles/${id}`)
      .toPromise()
      .then(response => response.json() as Article)
      .catch(err => console.log(err));
  }
}
