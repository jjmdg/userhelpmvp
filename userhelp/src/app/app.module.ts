import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule} from './app-routing.module';
import { StartComponent } from './start.component';
import { ArticleComponent } from './article.component';
import { ArticleSearchService } from './article-search.service';
import { ArticleCrudService } from './article-crud.service';
import { FieldsetModule, MenubarModule, ButtonModule, InputTextareaModule, InputTextModule } from 'primeng/primeng';
import { EditComponent } from './edit.component';

@NgModule({
  declarations: [
    AppComponent,
    StartComponent,
    ArticleComponent,
    EditComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    FieldsetModule,
    MenubarModule,
    ButtonModule,
    InputTextareaModule,
    InputTextModule
  ],
  providers: [ArticleSearchService, ArticleCrudService, EditComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
