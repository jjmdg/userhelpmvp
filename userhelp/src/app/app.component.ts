import { Component, OnInit } from '@angular/core';
import {MenubarModule,MenuItem} from 'primeng/primeng';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Celonis Userhelp';

  items: MenuItem[];

  ngOnInit() {
    this.items = [
                {label: 'Start', icon: 'fa-home', routerLink: ['']},
                {label: 'Topics', icon: 'fa-list-alt', routerLink: ['/topics']},
                {label: 'FAQ', icon: 'fa-question', routerLink: ['/faq']},
                {label: 'Contact', icon: 'fa-phone', routerLink: ['/contact']},
                {label: 'Admin', icon: 'fa-pencil', items: [
                  {label: 'Add article', icon: 'fa-plus', routerLink: ['/create']}
              ]}
            ];
  }
}
