import { Media } from './media';

export class Article {
  _id: string;
  title: string;
  content: string;
  created_date: Date;
//  media: Media[];
}
