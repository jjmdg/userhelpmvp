import { Component, OnInit, Input } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { ArticleSearchService } from './article-search.service';
import { ArticleCrudService } from './article-crud.service';
import { Article } from './article';
import { InputTextModule, ButtonModule, InputTextareaModule } from 'primeng/primeng';


@Component({
  selector: 'edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  @Input() article: Article;

  constructor(
    private articleSearchService: ArticleSearchService,
    private articleCrudService: ArticleCrudService,
    private router: Router) {}

  saveArticle(): void {
    if(this.article._id) {
      this.articleCrudService.update(this.article)
      .then(article => this.router.navigate(['/article', article._id]))
    } else {
    this.articleCrudService.create(this.article)
      .then(article => this.router.navigate(['/article', article._id]));
    }
  }

  ngOnInit(): void {
    this.article = new Article();
  }
}
