import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import { ArticleSearchService } from './article-search.service';
import { Article } from './article';
import { InputTextModule } from 'primeng/primeng';

import 'rxjs/add/observable/of';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Component({
  selector: 'start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})
export class StartComponent implements OnInit {
  articles: Observable<Article[]>;
  private searchTerms = new Subject<string>();

  constructor(
    private articleSearchService: ArticleSearchService,
    private router: Router) {}


  search(term: string): void {
    this.searchTerms.next(term);
  }

  goToArticle(id: string): void {
    this.router.navigate(['/article', id]);
  }

  ngOnInit(): void {
    this.articles = this.searchTerms
      .debounceTime(300)
      .distinctUntilChanged()
      .switchMap(term => term
        ? this.articleSearchService.search(term)
        : Observable.of<Article[]>([]))
      .catch(error => {
        // TODO: add real error handling
        console.log(error);
        return Observable.of<Article[]>([]);
      });
  }
}
