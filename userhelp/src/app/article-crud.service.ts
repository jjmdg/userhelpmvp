import { Injectable } from '@angular/core';
import { Http }       from '@angular/http';

import { Observable }     from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Article }           from './article';

@Injectable()
export class ArticleCrudService {
  articleRestUrl: string = 'api';

  constructor(private http: Http) {}

  create(article: Article): Promise<Article> {
    return this.http
               .post(`${this.articleRestUrl}/articles/`, article)
               .toPromise()
               .then(response => response.json() as Article)
               .catch(err => console.log(err));
  }

  update(article: Article): Promise<Article> {
    return this.http
               .put(`${this.articleRestUrl}/articles/${article._id}`, article)
               .toPromise()
               .then(response => response.json() as Article)
               .catch(err => console.log(err));
  }

}
